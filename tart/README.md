# Tart

## Prerequisites

1. This document assumes you have stored your AWS account credentials in `aws-vault`, in a profile named `runner`.

## Building the base image

1. Download the IPSW file for the version of macOS you need from https://ipsw.me/. Select Mac Mini M1 as the platform.

1. Make sure to verify the checksum.

1. Execute `tart create --from-ipsw=PATH_TO_DOWNLOADED_IPSW base`

1. Execute `tart run base`

1. Run the OSX setup

    - Select English as language
    - Select United States as region
    - Accept default language settings
    - Don't enable accessibility
    - Acknowledge Data & Privacy disclaimer
    - Do not use Migration Assistant
    - Don't setup an Apple ID
    - Accept the software license agreement
    - Create user:
      - name: `GitLab`
      - account name: `gitlab`
      - password: `gitlab`
    - Enable Location Services
    - Enable Apple Analytics and Share crash and usage data with developers
    - Do not enable Screen Time
    - Do not enable Ask Siri
    - Choose Light theme
    - Accept express setup (enables location and analytics)
    - Do not enable screen time nor siri

1. Do first boot customizations

    - Enable autologin so a graphical session is opened on boot, to enable users to launch apps such as the iOS simulator: [Apple guide](https://support.apple.com/en-us/HT201476)
    - Disable the screensaver by going to System Preferences > Desktop & Screensaver > Screensaver > Uncheck "Show screensaver after..." (so we don't waste cycles running floating graphics)
    - Disable Screen Lock by going to System Preferences > Security & Privacy > Uncheck "Require password X minutes after sleep or screensaver begins" (so the vm doesn't lock up after some time)
    - Disable Sleep by going to System Preferences > Energy Saver, and setting "Turn display off after" to Never, as well as checking "Prevent your mac from sleeping..."
    - In the System Preferences > Sharing, enable Screen Sharing, as well as Remote Login. Make sure to check "Allow full disk access to remote users"
    - Open `Terminal.app` and run `xcode-select --install` to install the Xcode Command Line Tools (requires GUI interraction)
    - Still in `Terminal.app`, run `sudo visudo`, find `%admin ALL=(ALL) ALL` and append a new line with `gitlab ALL=(ALL) NOPASSWD: ALL` to allow sudo without a password.

- Restart the machine to make sure to get the disk contents in a steady state, then shut it down
- Login to the registry `aws-vault exec runner -- aws ecr get-login-password --region eu-west-1 | tart login --password-stdin --username AWS 915502504722.dkr.ecr.eu-west-1.amazonaws.com`
- Push image to our registry `tart push base 915502504722.dkr.ecr.eu-west-1.amazonaws.com/macos/base:12.6.1`. Make sure to tag it with the full version of the OS. Say `12.0` if the version is `12`, so it is not overwritten at the next step later.
- Also create a copy of the tag pointing to the major version only:

```
MANIFEST=$(aws-vault exec runner -- aws ecr batch-get-image --region eu-west-1 --repository-name macos/base --image-ids imageTag=12.6.1 --query 'images[0].imageManifest' --output text)
aws-vault exec runner -- aws ecr put-image --region eu-west-1 --repository-name macos/base --image-tag 12 --image-manifest "$MANIFEST"
```

## Retag an image (take a snapshot, make a release)

The guide at https://docs.aws.amazon.com/AmazonECR/latest/userguide/image-retag.html has all the information.
