terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.32"
    }
    http = {
      source  = "hashicorp/http"
      version = "~> 3.1.0"
    }
    local = {
      source  = "hashicorp/local"
      version = "~> 2.2.3"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "~> 4.0.3"
    }
  }

  backend "http" {
    # Project # 19922160 = https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka
    address        = "https://gitlab.com/api/v4/projects/19922160/terraform/state/aws"
    lock_address   = "https://gitlab.com/api/v4/projects/19922160/terraform/state/aws/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/19922160/terraform/state/aws/lock"
    username       = "glpat"
    # password = "" # should be provided via `-backend-config="password=$GITLAB_PAT"` during `terraform init`
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }

  required_version = ">= 1.3.1"
}

# 8888888b.  8888888b.   .d88888b.  888     888 8888888 8888888b.  8888888888 8888888b.   .d8888b.
# 888   Y88b 888   Y88b d88P" "Y88b 888     888   888   888  "Y88b 888        888   Y88b d88P  Y88b
# 888    888 888    888 888     888 888     888   888   888    888 888        888    888 Y88b.
# 888   d88P 888   d88P 888     888 Y88b   d88P   888   888    888 8888888    888   d88P  "Y888b.
# 8888888P"  8888888P"  888     888  Y88b d88P    888   888    888 888        8888888P"      "Y88b.
# 888        888 T88b   888     888   Y88o88P     888   888    888 888        888 T88b         "888
# 888        888  T88b  Y88b. .d88P    Y888P      888   888  .d88P 888        888  T88b  Y88b  d88P
# 888        888   T88b  "Y88888P"      Y8P     8888888 8888888P"  8888888888 888   T88b  "Y8888P"

provider "aws" {
  region = "eu-west-1"

  # make sure we don't run this on a personal account
  allowed_account_ids = [
    "915502504722" # shared runner-saas gitlab sandbox account
  ]
}

# 8888888b.        d8888 88888888888     d8888
# 888  "Y88b      d88888     888        d88888
# 888    888     d88P888     888       d88P888
# 888    888    d88P 888     888      d88P 888
# 888    888   d88P  888     888     d88P  888
# 888    888  d88P   888     888    d88P   888
# 888  .d88P d8888888888     888   d8888888888
# 8888888P" d88P     888     888  d88P     888

data "aws_region" "current" {}

# 8888888b.  8888888888 .d8888b.   .d88888b.  888     888 8888888b.   .d8888b.  8888888888 .d8888b.
# 888   Y88b 888       d88P  Y88b d88P" "Y88b 888     888 888   Y88b d88P  Y88b 888       d88P  Y88b
# 888    888 888       Y88b.      888     888 888     888 888    888 888    888 888       Y88b.
# 888   d88P 8888888    "Y888b.   888     888 888     888 888   d88P 888        8888888    "Y888b.
# 8888888P"  888           "Y88b. 888     888 888     888 8888888P"  888        888           "Y88b.
# 888 T88b   888             "888 888     888 888     888 888 T88b   888    888 888             "888
# 888  T88b  888       Y88b  d88P Y88b. .d88P Y88b. .d88P 888  T88b  Y88b  d88P 888       Y88b  d88P
# 888   T88b 8888888888 "Y8888P"   "Y88888P"   "Y88888P"  888   T88b  "Y8888P"  8888888888 "Y8888P"

resource "aws_licensemanager_license_configuration" "license_config" {
  name                  = "macos-ami-building"
  description           = "Pass through configuration for macOS Host Resource Group"
  license_counting_type = "Core"
}

resource "aws_cloudformation_stack" "host_resource_group" {
  name = "macos-ami-building-resource-group"

  template_body = <<STACK
{
    "Resources" : {
        "HostResourceGroup": {
            "Type": "AWS::ResourceGroups::Group",
            "Properties": {
                "Name": "macos-ami-building-resource-group",
                "Configuration": [
                    {
                        "Type": "AWS::EC2::HostManagement",
                        "Parameters": [
                            {
                                "Name": "allowed-host-based-license-configurations",
                                "Values": [
                                    "${aws_licensemanager_license_configuration.license_config.arn}"
                                ]
                            },
                            {
                                "Name": "allowed-host-families",
                                "Values": [
                                    "mac2"
                                ]
                            },
                            {
                                "Name": "auto-allocate-host",
                                "Values": [
                                    "true"
                                ]
                            },
                            {
                                "Name": "auto-release-host",
                                "Values": [
                                    "true"
                                ]
                            }
                        ]
                    },
                    {
                        "Type": "AWS::ResourceGroups::Generic",
                        "Parameters": [
                            {
                                "Name": "allowed-resource-types",
                                "Values": [
                                    "AWS::EC2::Host"
                                ]
                            },
                            {
                                "Name": "deletion-protection",
                                "Values": [
                                    "UNLESS_EMPTY"
                                ]
                            }
                        ]
                    }
                ]
            }
        }
    },
    "Outputs" : {
        "ResourceGroupArn" : {
            "Description": "ResourceGroup Arn",
            "Value" : { "Fn::GetAtt" : ["HostResourceGroup", "Arn"] },
            "Export" : {
              "Name": "macos-ami-building-resource-group"
            }
        }
    }
}
STACK
}

resource "tls_private_key" "packer" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "packer" {
  key_name   = "packer"
  public_key = tls_private_key.packer.public_key_openssh
}

resource "local_sensitive_file" "packer_private_key" {
  content         = tls_private_key.packer.private_key_pem
  filename        = "${path.module}/packer.pem"
  file_permission = 0400
}

#  .d88888b.  888     888 88888888888 8888888b.  888     888 88888888888 .d8888b.
# d88P" "Y88b 888     888     888     888   Y88b 888     888     888    d88P  Y88b
# 888     888 888     888     888     888    888 888     888     888    Y88b.
# 888     888 888     888     888     888   d88P 888     888     888     "Y888b.
# 888     888 888     888     888     8888888P"  888     888     888        "Y88b.
# 888     888 888     888     888     888        888     888     888          "888
# Y88b. .d88P Y88b. .d88P     888     888        Y88b. .d88P     888    Y88b  d88P
#  "Y88888P"   "Y88888P"      888     888         "Y88888P"      888     "Y8888P"

output "region" {
  value = data.aws_region.current.name
}

output "host_resource_group_arn" {
  value = aws_cloudformation_stack.host_resource_group.outputs["ResourceGroupArn"]
}
output "license_configuration_arn" {
  value = aws_licensemanager_license_configuration.license_config.arn
}

output "keypair_name" {
  value = aws_key_pair.packer.key_name
}

output "keypair_private_key" {
  value = local_sensitive_file.packer_private_key.filename
}
