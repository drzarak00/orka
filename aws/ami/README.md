# macOS AMI build for Orka repository

This document describes the AWS infrastructure needed to build AMIs for use in this repository's macOS runners autoscaling group.

The infrastructure is provisioned with Terraform, with a state stored using the GitLab backend in this repository.

The AMIs are build by Packer.

## Components

- Infrastructure is provisioned in the shared runner-saas Sandbox Cloud instance, in the `eu-west-1` region.
- The Terraform state is stored using the GitLab backend on gitlab.com
- AWS License Manager is used to provision and release dedicated hosts for `mac2.metal` instances, as per https://aws.amazon.com/blogs/compute/implementing-autoscaling-for-ec2-mac-instances/
- Dedicated hosts are automatically released when no instance is running on them and they're more than 24h old
- A fixed SSH private key is generated and stored both locally as a file and in the Terraform state, so manual steps can be executed by an Engineer during the Packer build.
- A single Packer build is configured:
  - **runner**: It installs `gitlab-runner` as a service, and any tooling we require to run builds for this repository
- The Packer build require the contents of the Terraform outputs as variables

## Prerequisites

1. This document assumes you have stored your AWS account credentials in `aws-vault`, in a profile named `runner`.

1. `tfenv` should be installed and active in your shell

1. `VNC Viewer` (or alternative, but I tested a few and have had good luck with that one) should be installed for troubleshooting.

## Provision the supporting infrastructure with terraform

The supporting infrastructure to enable automatically managing `mac2.metal` dedicated hosts is provisioned by terraform.

1. Look at `main.tf` and familiarize yourself with its contents.

1. Create a GitLab Personal Access Token and make it available to the shell as `$GITLAB_PAT`.

1. Set-up your environment with `aws-vault exec runner -- terraform init -backend-config="password=$GITLAB_PAT"`. This will install the necessary terraform plugins.

1. If this is a new AWS account, you have to go to https://eu-west-1.console.aws.amazon.com/license-manager/home?region=eu-west-1#/gettingStarted once and enable License Manager IAM permissions.

1. Generate a plan with `aws-vault exec runner -- terraform plan -out tf.plan`.

1. If the plan makes sense, apply it with `aws-vault exec runner -- terraform apply tf.plan`

## Build the runner image with Packer

The runner build automates installing `gitlab-runner`, `tart` and other supporting tools.

1. Familiarize yourself with the contents of `main.pkr.hcl`. Please note that packer will require the outputs of the terraform module to be set as variables.

1. Execute `aws-vault exec runner --no-session -- packer build -timestamp-ui -only=runner.amazon-ebs.12-monterey-arm64-runner $(terraform output -json | jq -j -r 'to_entries[] | " -var \(.key)=\(.value | .value)"') main.pkr.hcl`. Note that the build is multi-hour long and so we force aws-vault to use long-term credentials.

1. The macOS instance will take up to 40 minutes to be provisioned and SSH to be available.

1. Packer will wait for you to do any manual checks on the AMI before snapshotting. Simply press enter in the usual case.

1. When Packer is shutting down the instance, AWS may run a scrubbing workflow on the instance as it is shut down, which could take up to 3 hours in the worst case. Be patient!

## Deploy a new AMI

TODO: See if we can provide a set of `aws-cli` commands to do this.

1. Regenerate the launch template by re-applying the Terraform. It will find the most recent AMI and use it. Alternatively, you can manually create a new version of the Launch Template in the console but it will probably be more tedious.

1. Signing to the AWS console for the shared runner sandbox cloud

1. Go to "EC2" > "Auto Scaling Groups" and find the `macos-internal-runner-XXX` ASG.

1. In the "Instance Refresh" tab, click "Start instance refresh"

1. Select a "Minimum healthy percentage" of 50% to have at least one instance InService during the changeover (as we currently have 2 instances). Select 0% to renew everything simultaneously.

1. In "Desired Configuration", enable "Update launch template". It will select the latest version by default.

1. Click "Start instance refresh"
