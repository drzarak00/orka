# Ansible Playbook: Setup OSX/iOS CI Environment

![MIT licensed][badge-license]

This playbook is meant to provision a MacStadium VM CI base image
(see inventory in `inventory/macstadium`) that can be used to manually scale
MacStadium CI hosts. It was inspired from MacStadium's macOS CI Setup
[Ansible Playbook][osx-playbook].

The target configuration is listed in `group_vars/all/*.yml`.

## dev-vm commands

| command                    | description                               |
| -------------------------- | ----------------------------------------- |
| `scripts/dev-vm status`    | Show the status of your VM                |
| `scripts/dev-vm new`       | Create a new dev VM                       |
| `scripts/dev-vm destroy`   | Destroy the VM to start again             |
| `scripts/dev-vm provision` | Run the local ansible playbook on your VM |
| `scripts/dev-vm ssh`       | SSH to your VM                            |
| `scripts/dev-vm spec`      | Run tests on your VM                      |

## How to

### Develop on the ansible playbooks

In order to successfuly run the playbook on your local machine during development,
the following steps are required:

1. Install dependencies: `ansible`, `jq`, `orka`, `socat`, `sshpass`, `pipenv`, `rvm`, `ruby`, `bundler`.

   ```shell
   brew tap gitlab/shared-runners git@gitlab.com:gitlab-org/ci-cd/shared-runners/homebrew.git
   brew install ansible jq orka socat gitlab/shared-runners/sshpass

   pip3 install pipenv
   pipenv install

   curl -sSL https://get.rvm.io | bash
   rvm install 2.7.1
   rvm use 2.7.1
   gem install bundler:2.2.10
   bundle install
   ```

1. Go through the [Orka CLI Quick Start](https://orkadocs.macstadium.com/docs/quick-start).
   You should end up with your own user credentials, and be able to use the CLI
   to list and create VMs as shown in the tutorial.

1. Generate a key for the [`mac-runner-provisioning` GCP service account](https://console.cloud.google.com/iam-admin/serviceaccounts/details/116590241271226936536?authuser=1&project=group-verify-df9383)
   and store it in the project root directory as a JSON file (named like `group-verify-df9383-*.json`)

1. Create your own VM by running `scripts/dev-vm new`. By default, it is named after
   your local unix account. A [few more commands](#dev-vm-commands) exist.
   The following provisions a fresh Big Sur machine, on which you want to run the toolchain role.
   It will create the vm on a `gitlab` orka node, wait for it to boot, then copy your public SSH key.

   ```shell
   scripts/dev-vm --base-image macos-11-90G-ssh.img --role toolchain --toolchain-version big-sur new
   ```

1. To SSH into your new VM, use `scripts/dev-vm ssh`.

   ```shell
   scripts/dev-vm --base-image macos-11-90G-ssh.img --role toolchain --toolchain-version big-sur ssh
   ```

1. To run ansible against your new VM, use `scripts/dev-vm provision`.

   ```shell
   # you need to add `--xcode-version 12` for the xcode roles
   scripts/dev-vm --base-image macos-11-90G-ssh.img --role toolchain --toolchain-version big-sur provision
   ```

   Note: requires a private key to be present at `~/.ssh/id_rsa` or `~/.ssh_ed25519`

1. To run the spec suite agains your VM, use `scripts/dev-vm spec`

   ```shell
   scripts/dev-vm --base-image macos-11-90G-ssh.img --role toolchain --toolchain-version big-sur spec
   ```

1. To run a specific rspec pattern, to target a line for example, you can do it manually with

   ```shell
   # get the IP and SSH port from the status
   scripts/dev-vm --base-image macos-11-90G-ssh.img --role toolchain --toolchain-version big-sur status

   export TARGET_HOST=10.221.188.128
   export TARGET_PORT=8824
   export TOOLCHAIN_VERSION=big-sur
   bundle exec rspec spec/package_managers/homebrew_spec.rb:43
   ```

### Release an image to production

1. Find the [latest `main` pipeline](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/pipelines?page=1&scope=all&ref=main):

1. Run one or more manual `release` jobs. This will save images named `ork-11-12-202104011023.img` in the orka image store.

1. Update your environment's `values.yaml` in the [`shared-runners/macos`](https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos) repository and follow the [Deploying the Helm release](https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos/runbooks/deploying-the-helm-release.md) runbook.

### Create a new base OS image

#### Build the image starting with empty disk and ISO installation

- Assuming `Catalina.iso` exists in the ISO storage (check using `orka iso list`)
- Generate an image `orka image generate --image macos-10.15-90G.img --size 90G -y`
- Create the VM `orka vm create --vm catalina-ssh --base-image macos-10.15-90G.img -c 12 --iso Catalina.iso --vnc`
- Connect to the machine with VNC
- Run the OSX setup
  - Select English as language
  - Go to Disk Utility
  - Erase "QEMU HARDDISK MEDIA" (there are two, pick the one that's 90G) using default settings:
    - before Big Sur: MacOS Extended (Journaled), GUID Partition Map and name it "Macintosh HD"
    - after Big Sur: APFS, GUID Partition Map and name it "Macintosh HD"
  - Quit Disk Utility and select re-install MacOs
  - Accept the software license agreement
  - Install MacOS to "Macintosh HD"
  - Select United States as region
  - Select US as keyboard layout
  - Skip Apple ID setup
  - Accept software license agreement
  - Create user:
    - name: `Gitlab`
    - account name: `gitlab`
    - password: `gitlab`
  - Accept express setup (enables location and analytics)
  - Do not enable screen time nor siri
- If asked to identify the keyboard, select ANSI
- Restart the machine to make sure to get the disk contents in a steady state, then shut it down
- Commit the newly installed disk contents to the snapshot: `orka image commit -v $VMID`
- Purge the VM: `orka vm purge -v $VMID`
- Go to the next point to set up the disk with our customisations

#### Build our base image starting with an installation snapshot

- The previous process went through the ISO installation and left us a pristine snapshot we can re-use safely to save time.
- Create a new VM `orka vm create --vm catalina-ssh --base-image macos-10.15-90G.img -c 12 --vnc`
- Login with user `gitlab` and password `gitlab`
- Install all software updates by going to the terminal and running `sudo softwareupdate -i --all --restart`
- Retry the updates in case it finds more after the first batch
- Install Xcode command line tools with `xcode-select --install`
- Enable SSH by going to System Preferences > Sharing > Click on Remote Login (so we can connect to the vms from the runner)
- Enable autologin so a graphical session is opened on boot, to enable users to launch apps such as the iOS simulator: [Apple guide](https://support.apple.com/en-us/HT201476)
- Disable the screensaver by going to System Preferences > Desktop & Screensaver > Select Start After Never at the bottom (so we don't waste cycles running floating graphics)
- Disable Screen Lock by going to System Preferences > Security & Privacy > Uncheck "Require password X minutes after sleep or screensaver begins" (so the vm doesn't lock up after some time)
- Disable Sleep by going to System Preferences > Energy Saver, and setting both computer and display sleep to "Never", as well as unchecking "Put hard disks to sleep when possible"
- Restart the machine to make sure to get the disk contents in a steady state, then shut it down
- Delete the existing base image `orka image delete --image macos-10.15-90G-ssh.img` (ensure no prod pipeline is using it at the moment)
- Save the image to its final identifier `orka image save -v $VMID -b macos-10.15-90G-ssh.img`
- Purge the VM: `orka vm purge -v $VMID`
<!-- markdownlint-enable MD013 MD044 -->

### Add a new macOS major version

1. Add `.macos_12` key in `.gitlab/ci/_common.gitlab-ci.yml`

1. Add jobs in `.gitlab/ci/{toolchain,xcode,runner,brew,release}.gitlab-ci.yml`

1. Add a toolchain file in `toolchain/`. Copy it from the previous release,
   and update all the tagged versions to be current. Note that [our policy](https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos/-/tree/master#macos-and-xcode-version-support)
   is to update the versions until the first .1 release of macOS is made available.

1. Make sure the tests pass and profit!

### Security updates

Images are frozen after the first patch release to avoid breaking customers with toolchain changes.
But our [image update policy](https://docs.gitlab.com/ee/ci/runners/saas/macos/environment.html#image-update-policy)
allows for making security updates to images in `maintenance` mode and `frozen` mode when unavoidable.
Security updates come from several channels:

1. MacOS
1. XCode
1. Homebrew
1. ASDF

MacOS updates can be installed on a regular cadence, but the other updates should only be installed when a specific security issue is being addressed.

#### Installing MacOS updates

1. Get the image name you want to update with `orka image list`. Generally it is the base OS snapshot (`macos-11-90G-ssh.img`) and the relevant pipeline snapshot (`toolchain-11-p.280311301.img` or `xcode-11-12-p.280311301.img` depending on OS version).

1. Create a VM with `scripts/dev-vm --base-image $IMAGE --copy-ssh-id false new`

1. SSH to the machine `scripts/dev-vm ssh`

1. Execute `sudo softwareupdate --install --all --restart`. Wait for it to do its thing, including restart, and for SSH to become available again.
   Do not change anything else on the machine as it would break reproducibility of the snapshot

1. Shutdown the VM `sudo shutdown -h now`

1. Wait one minute for it to shutdown and execute `scripts/dev-vm commit`. This can fail if VMs are using the base image, so they need to be killed.

1. Destroy the VM with `scripts/dev-vm destroy`

1. The image is now up-to-date

#### Installing XCode updates

We only install XCode security updates when they are available as a minor version upgrade for the current major version. 
For example, 12.1 -> 12.2.

1. Xcode installation packages are not freely available. You need to login to
   [the developer portal](https://developer.apple.com/download/more/) with your
   Apple ID to download the xcode `.xip` installation file.

1. Upload the `.xip` file to [our bucket in GCS](https://console.cloud.google.com/storage/browser/mac-runners;tab=objects?forceOnBucketsSortingFiltering=false&authuser=1&project=group-verify-df9383&prefix=&forceOnObjectsSortingFiltering=false)

1. Update the version mapping in `group_vars/all/xcode.yml`

1. Make sure the tests pass and profit!

#### Installing Homebrew updates

When you install a security update for a specific formula (package), you must know the version to target.
Usually a CVE will link to a repository commit and merge from which you can deduce which patch branches have the change.

Homebrew regularly updates formulae to the current major and minor versions so not all patch versions are present in the repository history.
You can choose the closest version which might be several minor versions ahead.
Homebrew does not maintain previous versions so upgrading a single formula requires extracting from a local tap (checking out a version of the formula file from repository history).

The [orca Ansible homebrew playbook](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/blob/9c4c70123e3af90936e41118973abb9ad0327a8f/roles/package_managers/tasks/homebrew.yml#L43) allows specifying a historical formula, version, and tap to extract ([example](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/blob/9c4c70123e3af90936e41118973abb9ad0327a8f/toolchain/monterey.yml#L31-35))

Extracting a formula version creates a file like `Formula/git@2.39.2.rb`. This version can then be [referenced](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/blob/9c4c70123e3af90936e41118973abb9ad0327a8f/toolchain/monterey.yml#L83) in the list of homebrew formula to install.

1. Extract and update the Homebrew formula version.

1. Submit your changes.

1. Build all images.

1. Update the image mapping in [`values-orka-beta.yaml`](https://gitlab.com/gitlab-org/ci-cd/shared-runners/macos/-/blob/a81b0ed22d659cf6e9a61b4e0bb42de4282b1e7f/values-orka-beta.yaml#L739-746).

#### Installing ASDF updates

ASDF plugins (packages) are identified by version.
You must know which version contains the security update.

1. Set the [global version](https://gitlab.com/gitlab-org/ci-cd/shared-runners/images/macstadium/orka/-/blob/9c4c70123e3af90936e41118973abb9ad0327a8f/toolchain/big-sur.yml#L64).

1. Submit your changes.

1. Build all images.

1. Update the image mapping.

### Rotate the production VM password

1. On an existing VM, change the password of the user.

1. Then, go into System Preferences > Users & Groups > Login Options and disable
   then re-enable Automatic Login. This regenerates the `/etc/kcpassword` file
   on disk, which macOS uses to start a session with auto-login.

1. Get the hexdump of this file to put in the CI variables with:

   ```shell
   sudo hexdump -ve '1/1 "%.2x"' /etc/kcpassword
   ```

1. Update the CI variables in this repository to generate new production images

   - `$ANSIBLE_USER_UPDATED_PASSWORD`
   - `$ANSIBLE_USER_UPDATED_KCPASSWORD_HEX`

1. Then follow the [Release an image to production](#release-an-image-to-production)
   guide. Don't forget to update the password in the Kubernetes secrets of your environment
   before deploying the Helm release.

## Xcode & SDK version support

### Current Xcode supported versions

The public docs have [a list of the currently supported versions](https://docs.gitlab.com/ee/ci/runners/saas/macos/environment.html#available-images)

<p>
<details>
<summary>See Xcode versions supporting data.</summary>

### Supporting data

At the time of writing (12/2020), this table lists the latest versions of every Apple OS, their release date, and whether any hardware device is stuck at this OS version.
This helps inform which Xcode version we support.

This data has been compiled using [Mactracker](https://mactracker.ca/) and [Wikipedia](https://en.wikipedia.org/wiki/Xcode).

The necessity of building software with old versions of Xcode, rather than using the [deployment target setting](https://help.apple.com/xcode/mac/current/#/deve69552ee5), is explained by a customer [here](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/6564#note_462485395).

#### macOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| Lion          | 10.7.5         | 2012 | 4.6.3             | iMac (Late 2006), Mac Mini (Mid 2007), Macbook (Late 2008), Macbook Pro (Late 2006), Xserve (Early 2008)                       |
| Mountain Lion | 10.8.5         | 2013 | 5.1.1             | -                                                                                                                              |
| Mavericks     | 10.9.5         | 2014 | 6.4               | -                                                                                                                              |
| Yosemite      | 10.10.5        | 2015 | 6.4               | -                                                                                                                              |
| El Capitan    | 10.11.6        | 2016 | 7.3.1             | iMac (Mid 2009), Mac mini (Late 2009), Macbook (Mid 2009), Macbook Air (Mid 2009), Macbook Pro (Mid 2009), Xserve (Early 2009) |
| Sierra        | 10.12.6        | 2017 | 8.3.3             | -                                                                                                                              |
| High Sierra   | 10.13.6        | 2018 | 9.4.1             | iMac (Late 2011), Mac mini (Mid 2011), Macbook (Mid 2010), Macbook Air (Mid 2011), Macbook Pro (Late 2011)                     |
| Mojave        | 10.14.6        | 2019 | 10.3              | Mac Pro (Mid 2012)                                                                                                             |
| Catalina      | 10.15.7        | 2020 | 11.7 (also 12.1)  | iMac (Late 2013), Mac Mini (Late 2012), Macbook Air (Mid 2012), Macbook Pro (Early 2013)                                       |
| Big Sur       | 11.1           | 2020 | 12.4              | -                                                                                                                              |

#### iOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 6             | 6.1.6          | 2014 | 4.6.3             | iPhone 3GS, iPod Touch (4th gen)                                                                                               |
| 7             | 7.1.2          | 2014 | 5.1.1             | iPhone 4, Apple TV (2nd gen)                                                                                                   |
| 8             | 8.4.2          | 2016 | 6.4               | Apple TV (3rd gen)                                                                                                             |
| 9             | 9.3.6          | 2019 | 7.3.1             | iPhone 4s, iPad 2, iPad (3rd Gen), iPad mini; iPöd Touch (5th Gen)                                                             |
| 10            | 10.3.4         | 2019 | 8.3.3             | iPhone 5, iPhone 5c, iPad (4th gen)                                                                                            |
| 11            | 11.4.1         | 2018 | 9.4.1             | -                                                                                                                              |
| 12            | 12.5           | 2020 | 10.3              | iPhone 6, iPhone 6s, iPod Touch (6th Gen), iPad Air, iPad Mini 2, iPad Mini 3                                                  |
| 13            | 13.7           | 2020 | 11.7              | -                                                                                                                              |
| 14            | 14.4           | 2020 | 12.4              | -                                                                                                                              |

#### iPad OS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 13            | 13.7           | 2020 | 11.7              | -                                                                                                                              |
| 14            | 14.4           | 2020 | 12.4              | -                                                                                                                              |

#### watchOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 1             | 1.0.1          | 2015 | ?                 | -                                                                                                                              |
| 2             | 2.2.2          | 2016 | 7.3.1             | -                                                                                                                              |
| 3             | 3.2.3          | 2017 | 8.3.3             | -                                                                                                                              |
| 4             | 4.3.2          | 2018 | 9.4.1             | Watch (1st gen)                                                                                                                |
| 5             | 5.3.7          | 2020 | 10.3              | -                                                                                                                              |
| 6             | 6.2.9          | 2020 | 11.7              | Watch (Series 1 & Series 2)                                                                                                    |
| 7             | 7.2            | 2020 | 12.4              | -                                                                                                                              |

#### tvOS

| Version       | Latest release | Year of latest release | Supplied by Xcode | Examples of devices stuck at this level                                                                                        |
|---------------|----------------|------|-------------------|--------------------------------------------------------------------------------------------------------------------------------|
| 9             | 9.2.2          | 2016 | 7.3.1             | -                                                                                                                              |
| 10            | 10.2.2         | 2017 | 8.3.3             | -                                                                                                                              |
| 11            | 11.4.1         | 2018 | 9.4.1             | -                                                                                                                              |
| 12            | 12.4.1         | 2019 | 10.3              | -                                                                                                                              |
| 13            | 13.4.8         | 2020 | 11.7              | -                                                                                                                              |
| 14            | 14.3           | 2020 | 12.4              | -                                                                                                                              |

</details>
</p>

## CI user

We use the [orka
API](https://orkadocs.macstadium.com/docs/api-quick-start) to provision
the VMs needed to build our images. A specific user inside of orka is
used to create these machines, with the username `ci@gitlab.com`. The
password of this user can be found in the 1Password vault `Verify` with
the name of `orka CI user`.

Use the [create
token](https://documenter.getpostman.com/view/6574930/S1ETRGzt?version=latest#3b0726f3-9473-4d42-bb3e-dfb242284cd2)
endpoint to create a new JWT token, and set it to `ORKA_API_TOKEN`
inside of the CI/CD settings.

## Roles overview

### `ci_user`

Sets up a CI user.

### `os_prepare`

Sets up base OS settings such as timezone.

### `os_updates`

Installs latest OS updates

### `package_managers`

Installs Homebrew, ASDF, and supporting packages such as developer tools,
languages, ...

### `release`

Updates the password to the production password

### `xcode`

Installs Xcode. Based on [ansible-role-xcode](https://github.com/palmerc/ansible-role-xcode).

#### GC storage

The xip file is located in the [`mac-runners` bucket](https://console.cloud.google.com/storage/browser/mac-runners;tab=objects?project=group-verify-df9383&prefix=),
and accessed through the [mac-runner-provisioning service account](https://console.cloud.google.com/iam-admin/serviceaccounts/details/116590241271226936536?project=group-verify-df9383).

The `GCS_KEYFILE` environment variable must contain the JSON string for
a service account key, downloaded from the Google Cloud dashboard.

### `gitlab_runner`

Downloads and installs the GitLab Runner binary

## Variables

The playbook will need several variables in order to work correctly.
Their default values can be found in [`group_vars/all/*.yml`](group_vars/all/).

```yml
    ansible_user: gitlab
```

The default user name to use for installing and configuring the needed tooling.

```yml
    ansible_become_password: ""
```

The password used to gain sudo access.

```yml
    ansible_user_updated_password: ""
```

The updated password to be used for the `ansible_user`, used by the
`release` role.
This is the password that must be specified in `ansible_become_password`
when reconnecting after the `release` role has finished executing.

### Output image names

The following is an overview of the names of the output image created
by each job, and used as the base image by the job in the subsequent stage:

Note that image names have a char length limit of 29, including extension.

- `toolchain-10.13.img`: base OS image + toolchain playbook
- `xcode-10.13-8.img`: toolchain image + xcode playbook
- `runner-10.13-8.img`: xcode image + runner playbook
- `brew-10.13-8.img`: runner image + up to date brew
- `ork-10.13-8-202104011230.img`: runner image + production password

For merge requests, the prefix `mr-$CI_MERGE_REQUEST_IID` is added.

## License

[MIT](./LICENSE)

## Author Information

This playbook was created in 2020 by GitLab.

### Maintainer(s)

- [Adrien Kohlbecker](https://gitlab.com/akohlbecker)
- [Pedro Pombeiro](https://gitlab.com/pedropombeiro)

[badge-license]: https://img.shields.io/badge/License-MIT-green.svg
[osx-playbook]: https://github.com/macstadium/ansible-playbook-osx-ci-setup
