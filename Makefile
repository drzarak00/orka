SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -euc
CPU_COUNT ?= 8

lint:
	@ pipenv run yamllint --version && pipenv run yamllint .
	@ pipenv run ansible-lint --version && pipenv run ansible-lint site.yml
	@ pipenv run ansible-playbook site.yml -i inventory/local --syntax-check

docs-lint:
	@ markdownlint --config .markdownlint.json '*.md'

docker: export BUILD_IMAGE ?= "gitlab-org/ci-cd/shared-runners/images/macstadium/orka/ci"
docker:
	@ docker build -t $(BUILD_IMAGE) -f ./dockerfiles/ci/Dockerfile .

_orka-check_args:
	@if [ -z "$(ORKA_API_URL)" ]; then echo "Please define ORKA_API_URL"; exit 1; fi
	@if [ -z "$(ORKA_VM_NAME)" ]; then echo "Please define ORKA_VM_NAME"; exit 1; fi

orka-check-vm: _orka-check_args
	@ if [ $(shell inventory/orka --host "$(ORKA_VM_NAME)" | jq length || echo 0) -ne 1 ]; then echo "'$(ORKA_VM_NAME)' VM not found!"; exit 1; fi

orka-create-vm: _orka-check_args
	@ echo "Creating '$(ORKA_VM_NAME)' VM from $(BASE_IMAGE_NAME)..."
	./scripts/ci/orka POST resources/vm/create orka_vm_name="$(ORKA_VM_NAME)" \
		orka_base_image="$(BASE_IMAGE_NAME)" orka_image="$(ORKA_VM_NAME)" \
		orka_cpu_core:=$(CPU_COUNT) vcpu_count:=$(CPU_COUNT) io_boost:=$(IO_BOOST)

orka-deploy-vm: TMPFILE ?= $(shell mktemp)
orka-deploy-vm: _orka-check_args
	@ echo "Deploying '$(ORKA_VM_NAME)'"
	./scripts/ci/orka POST resources/vm/deploy orka_vm_name="$(ORKA_VM_NAME)" vnc_console:=true | tee $(TMPFILE)

orka-provision-toolchain-vm: orka-check-vm
	@ # Provision toolchain VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with toolchain..."
	pipenv run ansible-playbook toolchain.yml -i inventory/orka -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e toolchain_version="$(TOOLCHAIN_VERSION)"

orka-provision-xcode-vm: _orka-check_args
	@ # Provision xcode VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with xcode..."
	pipenv run ansible-playbook xcode.yml -i inventory/orka -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e xcode_version="$(XCODE_VERSION)"

orka-provision-brew-vm: _orka-check_args
	@ # Provision brew VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with brew..."
	pipenv run ansible-playbook brew.yml -i inventory/orka -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e xcode_version="$(XCODE_VERSION)" \
		-e toolchain_version="$(TOOLCHAIN_VERSION)"

orka-provision-runner-vm: _orka-check_args
	@ # Provision runner VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with runner..."
	pipenv run ansible-playbook runner.yml -i inventory/orka -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e xcode_version="$(XCODE_VERSION)"

orka-provision-release-vm: _orka-check_args
	@ # Provision release VM with Ansible script
	@ echo "Provisioning '$(ORKA_VM_NAME)' ($(VM_ID)) with release..."
	pipenv run ansible-playbook release.yml -i inventory/orka -l "$(ORKA_VM_NAME)" \
		-e ansible_user="$(ANSIBLE_USER)" -e ansible_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_become_password="$(ANSIBLE_USER_SEED_PASSWORD)" \
		-e ansible_user_updated_password="$(ANSIBLE_USER_UPDATED_PASSWORD)" \
		-e ansible_user_updated_kcpassword_hex="$(ANSIBLE_USER_UPDATED_KCPASSWORD_HEX)"

orka-check-config-exists:
	./scripts/ci/orka GET resources/vm/configs | jq -e '.configs[].orka_vm_name | index( "$(ORKA_VM_NAME)" )  | select(. != null)'

orka-delete-vm-image: _orka-check_args
	@ echo "Deleting the $(OUTPUT_IMAGE_NAME) image..."
	./scripts/ci/orka POST resources/image/delete image="$(OUTPUT_IMAGE_NAME)"

orka-check-image-exists:
	@ echo "Checking if the $(OUTPUT_IMAGE_NAME) image exists..."
	./scripts/ci/orka GET resources/image/list | jq -e '.images | index( "$(OUTPUT_IMAGE_NAME)" )'

orka-reboot-vm: _orka-check_args
	pipenv run ansible "$(ORKA_VM_NAME)" -i inventory/orka --user "$(ANSIBLE_USER)" -e ansible_become_password="$(ANSIBLE_PASSWORD)" \
  	-e ansible_password="$(ANSIBLE_PASSWORD)" --become --module-name reboot --args "reboot_timeout=1800"

orka-force-reboot-vm: _orka-check_args
	./scripts/ci/orka POST resources/vm/exec/stop orka_vm_name="$(VM_ID)" && \
	sleep 5 && \
	./scripts/ci/orka POST resources/vm/exec/start orka_vm_name="$(VM_ID)"

orka-save-vm-image: orka-check-vm
	@ echo "Saving image from '$(ORKA_VM_NAME)' ($(VM_ID)) VM to $(OUTPUT_IMAGE_NAME) image..."
	TIMEOUT=1800 ./scripts/ci/orka POST resources/image/save orka_vm_name="$(VM_ID)" new_name="$(OUTPUT_IMAGE_NAME)"

orka-purge-vm: _orka-check_args
	./scripts/ci/orka DELETE resources/vm/purge orka_vm_name="$(ORKA_VM_NAME)"
